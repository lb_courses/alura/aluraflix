import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './pages/Home';
import { BrowserRouter,  Switch, Route} from 'react-router-dom';
import Video from './pages/cadastro/Video';
import CadastroCategoria from './pages/cadastro/Categoria';

function  Pagina404 ()  { 
  return (
    <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column' }}>
    
    <p>
      <a href="/">Você pode jogar ou voltar pra home:</a>
    </p>
    <p>
      Ou <a href="https://www.youtube.com/watch?v=jOAU81jdi-c&list=PLTcmLKdIkOWmeNferJ292VYKBXydGeDej">aprender a fazer o jogo</a>
    </p>
    {/*
      Pessoal, quem quiser fazer o desafio do Flappy Bird, da pra usar esse iframe aqui: 
      - https://codepen.io/omariosouto/pen/pogmdGE

      E quem quiser programar o jogo:
      - https://www.youtube.com/watch?v=jOAU81jdi-c&list=PLTcmLKdIkOWmeNferJ292VYKBXydGeDej
    */}
    <iframe
      title="Flappy Bird Game"
      src="https://mariosouto.com/flappy-bird-devsoutinho/"
      width="340"
      height="600" />
  </div>
  )
}


ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Home}></Route>
      <Route path="/cadastro/video"  component={Video}></Route>
      <Route path="/cadastro/categoria"  component={CadastroCategoria}></Route>
      <Route component={Pagina404} />
    </Switch>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
